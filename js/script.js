const hargaTiket = {
  Jakarta: 100000,
  Surabaya: 150000,
  Bandung: 80000,
  Yogyakarta: 120000
};

function hitungTotalBayar() {
  const tujuan = document.getElementById('tujuan').value;
  const harga = hargaTiket[tujuan];
  const jumlahTiket = parseInt(document.getElementById('jumlahTiket').value);
  const member = document.getElementById('member').checked;
  let diskon = parseFloat(document.getElementById('diskon').value);

  let totalBayar = harga * jumlahTiket;

  if (member) {
      totalBayar *= 0.9; 
  }

  if (!isNaN(diskon) && diskon > 0) {
      totalBayar -= diskon;
  }

  diskon = (harga * jumlahTiket) - totalBayar;

  document.getElementById('harga').value = harga === undefined ? '' : `Rp ${harga}`;
  document.getElementById('diskon').value = `Rp ${diskon}`;
  document.getElementById('totalBayar').value = `Rp ${totalBayar}`;
}

document.addEventListener('DOMContentLoaded', function () {
  const tujuan = document.getElementById('tujuan').value;
  const harga = hargaTiket[tujuan];
  document.getElementById('harga').value = harga === undefined ? '' : `Rp ${harga}`;
});

document.getElementById('tujuan').addEventListener('change', function () {
  const tujuan = this.value;
  const harga = hargaTiket[tujuan];
  document.getElementById('harga').value = harga === undefined ? '' : `Rp ${harga}`;
});
